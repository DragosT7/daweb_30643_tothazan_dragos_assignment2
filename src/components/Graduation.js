import React from 'react';
import { useTranslation } from 'react-i18next';

function Graduation() {
  const { t, i18n } = useTranslation();
  return (
    <div>
      <div className="Graduation">
        <h3>{t('ProjectTitle.1')}: {t('PedestrianDetection.1')}</h3>
        <h3>{t('PedestrianDetection.2')}</h3>
      </div>
      <div className="footer">
        <h4>&copy;Copyright by Dragos Tothazan</h4>
      </div>
    </div>
  );
}

export default Graduation;
