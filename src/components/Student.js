import React, { Profiler } from 'react';
import { useTranslation } from 'react-i18next';

function Student() {
  const { t, i18n } = useTranslation();
  return (
    <div>
      <div className="Student">
        <h3>{t('LastName.1')}: Tothazan</h3>
        <h3>{t('FirstName.1')}: Dragos</h3>
        <h3>{t('Faculty.1')}: {t('Faculty.2')}</h3>
        <h3>{t('Year.1')}: 4</h3>
        <h3>{t('Group.1')}: 30643</h3>
        <h3>{t('Passions.1')}:</h3>
        <ul className="ul-style">
          <li>{t('Football.1')}</li>
          <li>{t('Fitness.1')}</li>
          <li>{t('Reading.1')}</li>
          <li>{t('Walking.1')}</li>
          <li>{t('Jogging.1')}</li>
          <li>{t('Movies.1')}</li>
          <li>{t('BoardGames.1')}</li>
        </ul>
      </div>
      <div className="ProfilePhoto">
        <img src="/profile_photo.jpeg" alt="profile_photo" width="306" height="408"></img>
      </div>

      <div className="footer">
        <h4>&copy;Copyright by Dragos Tothazan</h4>
      </div>
    </div>
  );
}

export default Student;
