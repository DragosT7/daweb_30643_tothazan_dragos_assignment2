import React from 'react';
import { useTranslation } from 'react-i18next';

function Coordinator() {
  const { t, i18n } = useTranslation();
  return (
    <div>
      <div className="Coordinator">
        <h3>{t('Function.1')}: Lector</h3>
        <h3>{t('LastName.1')} {t('Coordinator.1')}: Marginea</h3>
        <h3>{t('FirstName.1')} {t('Coordinator.1')}: Anca</h3>
        <h3>{t('TeachingArea.1')}: {t('TeachingArea.2')}</h3>
      </div>
      <div className="footer">
        <h4>&copy;Copyright by Dragos Tothazan</h4>
      </div>
    </div>
  );
}

export default Coordinator;
