import React from 'react';
import { Link, withRouter } from "react-router-dom"

import { useTranslation } from 'react-i18next';

function Navigation() {

  const { t, i18n } = useTranslation();

  function handleClick(lang) {
    i18n.changeLanguage(lang);
  }

  return (
    <nav>
      <div className="NavigationBar">
        <ul className="ul-style">
          <Link to="/">
            <li className="li-style">{t('Home.1')}</li>
          </Link>

          <Link to="News">
            <li className="li-style">{t('News.1')}</li>
          </Link>

          <Link to="Graduation">
            <li className="li-style">{t('Graduation.1')}</li>
          </Link>

          <Link to="Student">
            <li className="li-style">{t('Student.1')}</li>
          </Link>

          <Link to="Interests">
            <li className="li-style">{t('Interests.1')}</li>
          </Link>

          <Link to="Coordinator">
            <li className="li-style">{t('Coordinator.1')}</li>
          </Link>

          <Link to="Research">
            <li className="li-style">{t('Research.1')}</li>
          </Link>

          <Link to="Contact">
            <li className="li-style">{t('Contact.1')}</li>
          </Link>

          <button className="button-styleRomanian" onClick={() => handleClick('ro')}>
            RO&nbsp;
            <img src="/ro_flag.png" alt="ro_flag" width="12px" height="12px"></img>
          </button>

          <button className="button-styleEnglish" onClick={() => handleClick('en')}>
            EN&nbsp;
            <img src="/en_flag.png" alt="ro_flag" width="12px" height="12px"></img>
          </button>
        </ul>
      </div>
    </nav>

  );
}

export default Navigation;
