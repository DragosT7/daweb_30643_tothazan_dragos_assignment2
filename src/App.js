import React from 'react';
import './App.css';

import Contact from './components/Contact';
import Coordinator from './components/Coordinator';
import Graduation from './components/Graduation';
import Interests from './components/Interests';
import Navigation from './components/Navigation';
import News from './components/News';
import Research from './components/Research';
import Student from './components/Student';

import {
  BrowserRouter as Router,
  Switch, 
  Route
} from 'react-router-dom';

function App() {
  return (
    <Router>
      <div className="App">
        <Navigation/>
        <Switch>
          <Route path="/" exact component={Student}/>
          <Route path="/Contact" component={Contact} />
          <Route path="/Coordinator" component={Coordinator} /> 
          <Route path="/Graduation" component={Graduation} />
          <Route path="/Interests" component={Interests} />
          <Route path="/News" component={News} />
          <Route path="/Research" component={Research} />
          <Route path="/Student" component={Student} />
        </Switch>
      </div>
    </Router>
  );
}



export default App;
